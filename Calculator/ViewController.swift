//
//  ViewController.swift
//  Calculator
//
//  Created by Gabor L Lizik on 08/09/16.
//  Copyright © 2016 Gabor L Lizik. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var display: UILabel!
    
    @IBOutlet var sequenceOfOperandsAndOperations: UILabel!
    
    fileprivate var userIsInTheMiddleOfTyping = false
    
    @IBAction fileprivate func touchDigit(_ sender: UIButton) {
        let digit = sender.currentTitle!
        
        // Homework #2
        // is there a "." on the display
        if ((display.text?.range(of: ".")) != nil) {
            
            // is it a "." again?
            if digit == "." {
                return
            }
        }
        
        if userIsInTheMiddleOfTyping {
            let textCurrentlyInDisplay = display.text!
            display.text = textCurrentlyInDisplay + digit
        } else {
            display.text = digit
        }
        
        userIsInTheMiddleOfTyping = true
    }
    
    fileprivate var displayValue: Double {
        get {
            return Double(display.text!)!
        }
        set {
            display.text = String(newValue)
        }
    }
    
    var savedProgram: CalculatorBrain.PropertyList?
    
    @IBAction func save() {
        savedProgram = brain.program
    }
    
    
    @IBAction func restore() {
        if savedProgram != nil {
            brain.program = savedProgram!
            displayValue = brain.result
        }
    }
    
    @IBAction func storeInMemory() {
        brain.variableValues["M"] = displayValue
    }
    
    @IBAction func copyFromMemory() {
        brain.setOperand(variableName: "M")
        if brain.result == 0.0 {
            display.text = "0"
        } else {
        displayValue = brain.result
        }
    }
    
    @IBAction func Undo() {
        if userIsInTheMiddleOfTyping {
            if (display.text?.characters.count)! > 1 {
                display.text = display.text!.substring(to: display.text!.index(before: display.text!.endIndex))
            } else {
                display.text = "0"
                userIsInTheMiddleOfTyping = false
            }
        } else {
            if (display.text?.characters.count)! > 1 {
            let descriptionCopy = brain.description
            let undoneDescription = descriptionCopy.substring(to: descriptionCopy.index(before: descriptionCopy.endIndex))
            sequenceOfOperandsAndOperations.text = undoneDescription
            } else {
                display.text = "0"
                userIsInTheMiddleOfTyping = false
            }
        }
    }
    
    fileprivate var brain = CalculatorBrain()
    
    @IBAction fileprivate func performOperation(_ sender: UIButton) {
        if userIsInTheMiddleOfTyping {
            brain.setOperand(displayValue)
        }
        userIsInTheMiddleOfTyping = false
        if let mathematicalSymbol = sender.currentTitle {
            brain.performOperation(mathematicalSymbol)
        }
        displayValue = brain.result
        sequenceOfOperandsAndOperations.text = brain.isPartialResult ? brain.description + " ..." : brain.description + " ="
    }
    
    @IBAction func clear(_ sender: UIButton) {
        display.text = "0"
        userIsInTheMiddleOfTyping = false
        sequenceOfOperandsAndOperations.text = " "
        brain.variableValues.removeValue(forKey: "M")
        brain.reset()
    }
}

