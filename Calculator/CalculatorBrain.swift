//
//  CalculatorBrain.swift
//  Calculator
//
//  Created by Gabor L Lizik on 13/09/16.
//  Copyright © 2016 Gabor L Lizik. All rights reserved.
//

import Foundation

class CalculatorBrain {
    fileprivate var accumulator = 0.0
    private var internalProgram = [Any]()
    var variableValues = [String:Double]()
    
    func setOperand(_ operand: Double) {
        accumulator = operand
        internalProgram.append(operand)
    }
    
    func setOperand(variableName: String) {
        accumulator = variableValues[variableName] ?? 0
        sequenceOfOperandsAndOperations += variableName
        internalProgram.append(accumulator)
    }
    
    
    
    fileprivate var operations: Dictionary<String, Operation> = [
        "π"   : Operation.constant(M_PI),
        "e"   : Operation.constant(M_E),
        "±"   : Operation.unaryOperation({ -$0 }),
        "√"   : Operation.unaryOperation(sqrt),
        "cos" : Operation.unaryOperation(cos),
        "×"   : Operation.binaryOperation({ $0 * $1 }),
        "÷"   : Operation.binaryOperation({ $0 / $1 }),
        "+"   : Operation.binaryOperation({ $0 + $1 }),
        "−"   : Operation.binaryOperation({ $0 - $1 }),
        "="   : Operation.equals,
        "sin" : Operation.unaryOperation(sin),
        "x²"  : Operation.unaryOperation({ $0 * $0 }),
        "x⁻¹" : Operation.unaryOperation({ 1 / $0 })
    ]
    
    fileprivate enum Operation {
        case constant(Double)
        case unaryOperation((Double) -> Double)
        case binaryOperation((Double, Double) -> Double)
        case equals
    }
    
    fileprivate var sequenceOfOperandsAndOperations = " "
    
    fileprivate func history(symbol: String) {
        if symbol == "=" {
            if pending != nil {
                if isPartialResult {
                    isPartialResult = false
                    if constantOrUnaryWasLastOperation {
                        constantOrUnaryWasLastOperation = false
                    } else {
                        sequenceOfOperandsAndOperations += "\(accumulator)"
                    }
                } else {
                    sequenceOfOperandsAndOperations += "\(accumulator)"
                }
            } else {
                isPartialResult = false
            }
        } else {
            if isPartialResult {
                sequenceOfOperandsAndOperations += "\(accumulator) \(symbol)"
            } else {
                sequenceOfOperandsAndOperations += "\(symbol)"
                isPartialResult = true
            }
        }
    }
    
    var startNewHistory = false
    
    var description: String {
        get {
            return sequenceOfOperandsAndOperations
        }
    }
    
    var constantOrUnaryWasLastOperation = false
    
    func performOperation(_ symbol: String) {
        internalProgram.append(symbol)
        if let operation = operations[symbol] {
            switch operation {
            case .constant(let value):
                accumulator = value
                sequenceOfOperandsAndOperations += symbol
                constantOrUnaryWasLastOperation = true
            case .unaryOperation(let function):
                if pending != nil {
                    sequenceOfOperandsAndOperations += "\(symbol)(\(accumulator))"
                } else {
                    sequenceOfOperandsAndOperations = "\(symbol)(\(sequenceOfOperandsAndOperations))"
                }
                accumulator = function(accumulator)
                startNewHistory = true
                constantOrUnaryWasLastOperation = true
            case .binaryOperation(let function):
                if startNewHistory {
                    sequenceOfOperandsAndOperations = "\(accumulator) \(symbol)"
                    executePendingBinaryOperation()
                    pending = PendingBinaryOperationInfo(binaryFunction: function, firstOperand: accumulator)
                    startNewHistory = false
                } else {
                    history(symbol: symbol)
                    executePendingBinaryOperation()
                    pending = PendingBinaryOperationInfo(binaryFunction: function, firstOperand: accumulator)
                }
                
            case .equals:
                history(symbol: symbol)
                executePendingBinaryOperation()
            }
        }
    }
    
    fileprivate func executePendingBinaryOperation() {
        if pending != nil {
            accumulator = pending!.binaryFunction(pending!.firstOperand, accumulator)
            pending = nil
        }
    }
    
    fileprivate var pending: PendingBinaryOperationInfo?
    
    fileprivate struct PendingBinaryOperationInfo {
        var binaryFunction: (Double, Double) -> Double
        var firstOperand: Double
    }
    
    typealias PropertyList = Any
    
    var program: PropertyList {
        get {
            return internalProgram
        }
        set {
            clear()
            if let arrayOfOps = newValue as? [AnyObject] {
                for op in arrayOfOps {
                    if let operand = op as? Double {
                        setOperand(operand)
                    } else if let operation = op as? String {
                        if operations[op as! String] != nil {
                        performOperation(operation)
                        } else {
                            setOperand(variableName: op as! String)
                        }
                    }
                }
            }
        }
    }
    
    func clear() {
        accumulator = 0.0
        pending = nil
        internalProgram.removeAll()
    }
    
    var result: Double {
        get {
            return accumulator
        }
    }
    
    var isPartialResult = true
    
    func reset() {
        accumulator = 0.0
        sequenceOfOperandsAndOperations = ""
        pending = nil
        isPartialResult = true
        startNewHistory = false
        constantOrUnaryWasLastOperation = false
    }
}
